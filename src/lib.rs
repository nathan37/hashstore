#![deny(warnings)]

#[cfg(test)]
#[macro_use]
mod testutils;

pub mod b64;
mod hash;
mod hashspool;
mod hashstore;
mod unival;

// Library Public API:
#[cfg(not(test))]
pub use crate::hash::{Hash, Hasher, HASH_BYTES};
pub use crate::hashstore::{HashInserter, HashStore};
pub use crate::unival::UniqueValue;

pub const EMPTY_HASH: &'static str = "DldRwCblQ7Loqy6wYJnaodHl30d3j3eH-qtFzfEv46g";
